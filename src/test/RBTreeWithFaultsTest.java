package test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import src.RBTreeWithFaults;
import src.RBTreeWithFaults.RBNode;
import static org.junit.Assert.*;

/**
 *
 * @author Brandon Deen & Saad Subhani
 */
public class RBTreeWithFaultsTest {
    
    public RBTreeWithFaultsTest() {}
    
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    private static RBTreeWithFaults setUpTree(){
    	int[] keys = {20,30,40,50,10};
    	String[] vals = {"20","30","40","50","10"};
    	RBTreeWithFaults rbt = new RBTreeWithFaults();
    	
    	for(int i=0; i<keys.length; i++){
    		rbt.insert(keys[i], vals[i]);
    	}
    	
    	return rbt;
    }
    
    @Test
    public void testConstructor1() {
    	RBTreeWithFaults testTree = setUpTree();
        RBTreeWithFaults.RBNode node = testTree.getRoot();
        RBTreeWithFaults testTree2 = new RBTreeWithFaults(node);
               
        //RBTreeWithFaults.RBNode result = testTree.getRoot();
        //assertEquals(expKey, result);
       
        //fail("testConstructor1()");
    }

    @Test
    public void testGetRoot1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        //TODO change expResult 
        //RBTreeWithFaults.RBNode expResult = new RBTreeWithFaults.RBNode( rootVal, rootKey, parent );
        int expKey = 20; 
        String expVal = "t2";
       
        RBTreeWithFaults.RBNode result = testTree.getRoot();
        assertEquals(expKey, result);
       
        fail("testGetRoot1()");
    }

    @Test
    public void testEmpty1() {
        RBTreeWithFaults testTree = new RBTreeWithFaults();
        
        boolean expResult = true;
        boolean result = testTree.empty();
        assertEquals(expResult, result);
        
        //fail("testEmpty1()");
    }
    
    @Test
    public void testEmpty2() {
    	RBTreeWithFaults testTree = setUpTree();
         
        boolean expResult = false;       
        boolean result = testTree.empty();
        assertEquals(expResult, result);
        
        //fail("testEmpty2()");
    }

    @Test 
    public void testSearch1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String expResult = "10";
        String result = testTree.search(10);
        assertEquals(expResult, result);
        
        //fail("testSearch1()");
    }
    
    @Test 
    public void testSearch2() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String expResult = null;
        String result = testTree.search(5);
        assertEquals(expResult, result);
        
        //fail("testSearch2()");
    }
    
    @Test 
    public void testSearch3() {
    	RBTreeWithFaults testTree = new RBTreeWithFaults();
        
        String expResult = null;
        String result = testTree.search(5);
        assertEquals(expResult, result);
        
        //fail("testSearch3()");
    }

    @Test
    public void testInsert1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        int expResult = 2;
        int result = testTree.insert(1, "insertTest");
        assertEquals(expResult, result);
        
        //fail("testInsert1()");
    }

    @Test
    public void testInsert2() {
    	RBTreeWithFaults testTree = setUpTree();
        
        int expResult = -1;
        int result = testTree.insert(10, "t1");
        assertEquals(expResult, result);
        
        //fail("testInsert2()");
    }
    
    @Test
    public void testDelete1() {
    	RBTreeWithFaults testTree = setUpTree();
    	
        int expResult = 0;
        int result = testTree.delete(10);
        assertEquals(expResult, result); 
        
        //fail("testDelete1()");
    }
    
    @Test
    public void testDelete2() {
    	RBTreeWithFaults testTree = setUpTree();
        
        int expResult = -1;
        int result = testTree.delete(100);
        assertEquals(expResult, result);
        
        //fail("testDelete2()");
    }

    @Test
    public void testMin1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String expResult = "10";
        String result = testTree.min();
        assertEquals(expResult, result);
        
        //fail("testMin1()");
    }
    

    @Test
    public void testMax1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String expResult = "50";
        String result = testTree.max();
        assertEquals(expResult, result);
        
        //fail("testMax1()");
    }


    @Test
    public void testMaxValue1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String expResult = "50";
        String result = testTree.max();
        assertEquals(expResult, result);
        
        //fail("testMaxValue1()");
    }

    @Test
    public void testKeysToArray1() {
    	RBTreeWithFaults testTree = setUpTree();
       
        int[] expResult = {10,20,30,40,50};
        int[] result = testTree.keysToArray();
        assertArrayEquals(expResult, result);
        
        //fail("testKeysToArray1()");
    }
    
    public void testKeysToArray2() {
    	//int[] keys = {};
    	RBTreeWithFaults testTree = new RBTreeWithFaults();
    	
        int[] expResult = null;
        int[] result = testTree.keysToArray();
        assertArrayEquals(expResult, result);
        
        //fail("testKeysToArray2()");
    }

    @Test
    public void testValuesToArray() {
    	RBTreeWithFaults testTree = setUpTree();
        
        String[] expResult = {"20","30","40","50","10"};
        String[] result = testTree.valuesToArray();
        assertArrayEquals(expResult, result);
        
        //fail("testValuesToArray1()");
    }

    /* We shouldnt have to test this 
    @Test
    public void testPrint() {
        RBTreeWithFaults testTree = new RBTreeWithFaults();
        testTree.print();
    }*/
	
    @Test
    public void testSize1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        int expResult = 5;
        int result = testTree.size();
        assertEquals(expResult, result);
        
        //fail("testSize1()");
    }

    @Test
    public void testSizeCalc1() {
    	RBTreeWithFaults testTree = setUpTree();
        
        RBTreeWithFaults.RBNode node = testTree.getRoot();
        
        int expResult = 5;
        int result = RBTreeWithFaults.sizeCalc(node);
        assertEquals(expResult, result);
        
        //fail("testSizeCalc1()");
    }
}    
